%{

extern "C"
{
        int yylex(void);
        int yyerror(const char *s);
        int yywrap()
        {
                return 0;
        }

}

#include "nodes.h"
// using namespace std;

// The AST tree root
// *pgm root;

%}

%token NUMBER PLUS INDENT DEDENT NEWLINE END_OF_FILE ASSIGN NAME IF ELSE COLON ELIF FOR IN
%token FALSE TRUE NONE DSTAR MINUS DIV MOD STAR COMPOP NOT IS OR AND COMMA PASS RETURN
%token GLOBAL NONLOCAL AUGASSIGN

%union {
  float num;
  char *id;
  exp_node *expnode;
  list<statement *> *stmts_type;
  statement *st;
  pgm *prog;
}

%type <stmts_type> stmts
%type <prog> program

%%

program:  stmts end   { $$ = new pgm($1); };


stmts:  stmt | stmts stmt ;
stmt:   simple_stmt | compound_stmt | NEWLINE;


/* START: simple statements */
simple_stmt: small_stmt NEWLINE;        /* (';' small_stmt)* [';'] NEWLINE */
small_stmt: expr_stmt | pass_stmt | flow_stmt | global_stmt | nonlocal_stmt ;

expr_stmt: testlist_star_expr AUGASSIGN testlist  | testlist_star_expr test_list_assign_chain;
test_list_assign_chain: ASSIGN testlist_star_expr | test_list_assign_chain ASSIGN testlist_star_expr;
testlist_star_expr: testlist_star_list | testlist_star_list COMMA;
testlist_star_list:  test | testlist_star_list COMMA test;

pass_stmt:   PASS;

flow_stmt:   return_stmt;
return_stmt: RETURN | RETURN testlist;

global_stmt: GLOBAL namelist;

nonlocal_stmt: NONLOCAL namelist;
/* END: Expressions */



/* START: compoud statements - statements with blocks */
compound_stmt: if_stmt | for_stmt;

/* is there a cleaner way to do this? */
if_stmt: if_stmt_block | if_stmt_block else_stmt_block | if_stmt_block elif_stmt_blocks else_stmt_block | if_stmt_block elif_stmt_blocks;

if_stmt_block: IF test COLON block;
elif_stmt_blocks: elif_stmt_block | elif_stmt_blocks NEWLINE elif_stmt_block;
elif_stmt_block: ELIF test COLON block;
else_stmt_block: ELSE COLON block;

for_stmt: FOR exprlist IN testlist COLON block ;

block: NEWLINE INDENT stmts DEDENT;
/* END: compoud statements */



/* expr/test lists */
namelist:  NAME | namelist COMMA NAME;
exprlist: exprs | exprs COMMA;
testlist: tests | tests COMMA;
exprs:    expr | expr COMMA exprs;
tests:    test | test COMMA tests;


/* test statements */
test: or_test  /*| or_test IF or_test ELSE test; */    /* | lambdef */
/*test_nocond: or_test;                                /* | lambdef_nocond */
/* lambdef: 'lambda' [varargslist] ':' test
   lambdef_nocond: 'lambda' [varargslist] ':' test_nocond */
or_test: and_test | and_test OR or_test ;
and_test: not_test | not_test AND and_test ;
not_test: NOT not_test | comparison ;
comparison: expr { $$ = new list<exp_comparison *>();  $$->push_back($1); }
        | expr comp_op comparison { $$ = $2; $2->push_back($1) /* Fix ordering*/};
comp_op: COMPOP | IN | NOT IN | IS | IS NOT ;

/* Grammar for python expresssions  */
expr : arith_expr
/* expr: xor_expr ('|' xor_expr)*
   xor_expr: and_expr ('^' and_expr)*
   and_expr: shift_expr ('&' shift_expr)*
   shift_expr: arith_expr (('<<'|'>>') arith_expr)* */
arith_expr: term | term PLUS arith_expr | term MINUS arith_expr;
term: factor | factor STAR term | factor DIV term | factor MOD term;      /* add // operator */
factor: PLUS factor | MINUS factor | power ;                              /* ~ factor? */
power: atom | atom DSTAR factor ;                                         /* add trailer (()[].var)*/


/* atom */
atom:   NAME | NUMBER | NONE | TRUE | FALSE ;



end:    END_OF_FILE { exit(0); } ;




%%
