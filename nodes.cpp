#include <iostream>
#include <stdlib.h>
#include <string>
#include <map>
#include <list>
#include "nodes.h"

using namespace std;

// These are all the constructors.  They are all very similar so they are together
comparison::comparison()
single_comparison::single_comparison(string comp_op_in, exp_comparison *L_in, exp_comparison *R_in) {
  comp_op = comp_op_in; L = L_in; R = R_in;
}



  // the constructor for node links the node to its children,
  // and stores the character representation of the operator.
  operator_node::operator_node(exp_node *L, exp_node *R) {
    left    = L;
    right   = R;
  }

  
  number_node::number_node(float value) {
    num = value;
   }
  
void number_node:: print() {
  cout << num;
}


  id_node::id_node(string value) : id(value) {}

void id_node:: print() {
  cout << id;
}


// plus_node inherits the characteristics of node and adds its own evaluate function
  // plus_node's constructor just uses node's constructor
  plus_node::plus_node(exp_node *L, exp_node *R) : operator_node(L,R) {
  }

void plus_node:: print() {
  cout << "(";
  left->print();
  cout << " + ";
  right->print();
  cout << ")";
}



// minus_node inherits the characteristics of node and adds its own evaluate function
  minus_node::minus_node(exp_node *L, exp_node *R) : operator_node(L,R) {
  }

void minus_node:: print() {
  cout << "(";
  left->print();
  cout << " - ";
  right->print();
  cout << ")";
}


// times_node inherits the characteristics of node and adds its own evaluate function
  times_node::times_node(exp_node *L, exp_node *R) : operator_node(L,R) {
  }

void times_node:: print() {
  cout << "(";
  left->print();
  cout << " * ";
  right->print();
  cout << ")";
}



// divide_node inherits the characteristics of node and adds its own evaluate function

  divide_node::divide_node(exp_node *L, exp_node *R) : operator_node(L,R) {
  }

void divide_node:: print() {
  cout << "(";
  left->print();
  cout << " / ";
  right->print();
  cout << ")";
}

// unary_minus_node inherits the characteristics of node and adds its own evaluate function
unary_minus_node::unary_minus_node(exp_node *L) : exp(L) {}

void unary_minus_node:: print() {
  cout << "-";
  exp->print();
}


assignment_stmt::assignment_stmt(string name, exp_node *expression)
  : id(name), exp(expression) {}

void assignment_stmt::print() {
  cout << id << " = ";
  exp->print();
  cout << endl;
}

print_stmt::print_stmt (string name) : id(name) {}


pgm::pgm(list<statement *> *stmtList) : stmts_type(stmtList) {}


map<string, float> idTable;
