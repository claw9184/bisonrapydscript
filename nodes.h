#include <iostream>
#include <stdlib.h>
#include <string>
#include <map>
#include <list>

using namespace std;

class exp_node {
  public:
    float num;

    // print function for pretty printing an expression
    virtual void print() = 0;

};

class operator_node : public exp_node {
public:
    exp_node *left;
    exp_node *right;

  // the constructor for node links the node to its children,
  // and stores the character representation of the operator.
    operator_node(exp_node *L, exp_node *R);
};

class number_node : public exp_node {
  
public:
  number_node(float value);
  void print();
};

class unary_minus_node : public exp_node {
 protected:
  exp_node *exp;
 public:
  unary_minus_node(exp_node *exp);
  void print();
};

class id_node : public exp_node {
protected:
  string id;

public:
  id_node(string value);
  void print();
};

// plus_node inherits the characteristics of node and adds its own evaluate function
class plus_node : public operator_node {
  public:

  // plus_node's constructor just uses node's constructor
  plus_node(exp_node *L, exp_node *R);
  void print();
};


// minus_node inherits the characteristics of node and adds its own evaluate function
class minus_node : public operator_node {
  public:

  minus_node(exp_node *L, exp_node *R);
  void print();
};


// times_node inherits the characteristics of node and adds its own evaluate function
class times_node : public operator_node {
  public:

  times_node(exp_node *L, exp_node *R);
  void print();
};


// divide_node inherits the characteristics of node and adds its own evaluate function
class divide_node : public operator_node {
  public:

  divide_node(exp_node *L, exp_node *R);
  void print();
};




// STATEMENTS
class statement {
 public:
  virtual void print() {}
};

// catches pass
class pass_stmt : public statement {
  // Nothing to do for pass statements
};

// catches global, nonlocal, and return
class keyword_stmt : public statement {
 protected:
  string vars;
  string keyword;
};



class assignment_stmt : public statement {
 protected:
  string id;
  exp_node *exp;
 public:
  assignment_stmt(string name, exp_node *expression);
  void print();
};

class print_stmt: public statement {
 protected:
  string id;
 public:
  print_stmt(string id);
};


// These are complete sets of code that evaluate to something
//  such as test statements, or the right side of an expression
class molecule {
 //Nothing here, this is just used for list purposes
};

// Just used as a type really for expr and expr COMP expr
class exp_comparison {
 //Nothing here
};

class single_comparison: public exp_comparison {
 protected:
    string         comp_op;
    exp_comparison *L;
    exp_comparison *R;
};

class comparison: public exp_comparison{
 protected:
  list<exp_comparison *> *comparison_chain;
};


class pgm {
 protected:
  list<statement *> *stmts_type;
 public:
  pgm(list<statement *> *stmtlist);
};

// the object at the base of our tree
extern map<string, float> idTable;
extern pgm *root;
