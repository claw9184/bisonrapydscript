#include <iostream>
using namespace std;
#include <stdio.h>
extern int yyparse();
//extern "C" int yylex(); 
extern "C" FILE *yyin;

int main(int argc, char **argv)
{
  // open a file handle to a particular file:
  FILE *myfile = fopen("testfile.pyj", "r");
  // make sure it's valid:
  if (!myfile) {
	  cout << "I can't open a.snazzle.file!" << endl;
	  return -1;
  }
  // set lex to read from it instead of defaulting to STDIN:
  yyin = myfile;
  // lex through the input:
  //yylex();
	// parse through the input until there is no more:
	
	// do {
	yyparse();
	// } while (!feof(yyin));
  return 0;
}
