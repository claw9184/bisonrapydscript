%{
#include <stdlib.h>
#include <stdio.h>
#include "yacc.tab.h"
#define MAX_DEPTH 72

int nesting = 0 ;
int newline_processed = 1;
unsigned int indent_stack[MAX_DEPTH] ;
unsigned int level = 0 ;

unsigned int first = 1 ;

int process_indent(int direction, int indent);
int check_indent(int line) ;


%}


%%


(^[ ]*|(\n)[ ]*\n)       {/* Ignore blank lines. */}
(^[ ]*|(\n)[ ]*)         { 
                           // Look ahead to see if there are any characters
                           int num_spaces = yyleng;
                           if (yytext[0] == '\n') {
                             num_spaces -= 1;
                           }
                         
                           int direction = check_indent(num_spaces);
                           if (direction == 0){
                             // Simple newline
                             if (newline_processed == 1) {
                               return NEWLINE;
                             } else {
                               // this is part of a dedent line and the newline was already caught
                               newline_processed = 1;
                             }
                           } else {
                             // Reprocess the line until all indentation is caught
                             yyless(0);
                             if (newline_processed == 1) {
                               newline_processed = 0;
                               return NEWLINE;
                             } else {
                               process_indent(direction, num_spaces);
                               if (direction < 0) {
                                 return DEDENT;
                               } else {
                                 return INDENT;
                               }
                             }
                           }
                         }
[ ]*                     { /* Do nothing return SEP; */ }
"("                      {nesting++ ; }
")"                      {nesting-- ; }
":"                      return COLON;
","                      return COMMA;
("<"|">"|"=="|">="|"<="|"<>"|"!=")    return COMPOP;
("+="|"-=")              return AUGASSIGN;   /* |"-="|"*="|"/="|"%="|"&="|"|="|"^="|"<<="|">>="|"**="|"//=" */
"or"                     return OR;
"and"                    return AND;
"pass"                   return PASS;
"return"                 return RETURN;
"global"                 return GLOBAL;
"nonlocal"               return NONLOCAL;
"in"                     return IN;
"not"                    return NOT;
"is"                     return IS;
"="                      return ASSIGN;
"+"                      return PLUS;
"-"                      return MINUS;
"/"                      return DIV;
"%"                      return MOD;
"*"                      return STAR;
"**"                     return DSTAR;
"None"                   return NONE;
"True"                   return TRUE;
"False"                  return FALSE;
"if"                     return IF;
"else"                   return ELSE;
"elif"                   return ELIF;
"for"                    return FOR;
"bye"                    return END_OF_FILE;
[a-zA-Z_][a-zA-Z0-9_]*   return NAME;
0(x|X)[0-9a-fA-F]+       { return NUMBER; /* hex number */ }
0[0-7]+                  { return NUMBER; /* oct number */ }
[0-9]+                   { return NUMBER; /* base 10 number */ }

%%




int check_indent(int indent) {
  if (nesting) {
    /* Ignore indents while nested. */
    return 0;
  }

  if (indent == indent_stack[level]) {
    return 0;
  }

  if (indent > indent_stack[level]) {
    // assert(level+1 < MAX_DEPTH) ;
    return 1; /* Go 1 level deeper */
  }

  return -1 ;
}


int process_indent(int direction, int indent) {

  if (direction > 0) {
    // assert(level+1 < MAX_DEPTH) ;
    indent_stack[++level] = indent ;
  } else if (direction < 0) {
    --level ;
    // assert(level >= 0) ;
  }
}


int yyerror (const char *s)
{                                                     
    printf("Parser error %s \n ",s);
    return 0;
}
/*
int main(int argc, char **argv)
{
  // open a file handle to a particular file:
  FILE *myfile = fopen("testfile.pyj", "r");
  // make sure it's valid:
  // set lex to read from it instead of defaulting to STDIN:
  yyin = myfile;
  // lex through the input:
  //yylex();
	// parse through the input until there is no more:
	
	int i = 40;
	do {
	  i--;
		yylex();
	} while (i);
  return 0;
}
*/
